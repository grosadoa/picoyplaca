package com.rp3.picoyplaca.Options.History;

public class HistoryModel {

    private Integer image;
    private String type;
    private String licenseplate;
    private String date;
    private String time;
    private String contravetion;

    public HistoryModel(Integer image, String type, String licenseplate, String date, String time, String contravetion) {
        this.image = image;
        this.type = type;
        this.licenseplate = licenseplate;
        this.date = date;
        this.time = time;
        this.contravetion = contravetion;
    }

    public Integer getImage() {
        return image;
    }

    public void setImage(Integer image) {
        this.image = image;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLicenseplate() {
        return licenseplate;
    }

    public void setLicenseplate(String licenseplate) {
        this.licenseplate = licenseplate;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getContravetion() {
        return contravetion;
    }

    public void setContravetion(String contravetion) {
        this.contravetion = contravetion;
    }
}
