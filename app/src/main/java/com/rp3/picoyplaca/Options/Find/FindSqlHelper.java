package com.rp3.picoyplaca.Options.Find;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.rp3.picoyplaca.Utils.Constantes;

public class FindSqlHelper extends SQLiteOpenHelper {

    String sqlCreate = "CREATE TABLE "+Constantes.TABLE+" ("+ Constantes.IMAGEFIND +" INT,"+ Constantes.TYPEFIND +" TEXT, "+ Constantes.LICENSEPLATEFIND +" TEXT,"+ Constantes.DATEFIND +" TEXT,"+Constantes.TIMEFIND+" TEXT,"+Constantes.CONTRAVENTIONFIND+" TEXT)";

    public FindSqlHelper(Context contexto, String name,
                         SQLiteDatabase.CursorFactory factory, int version) {
        super(contexto, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(sqlCreate);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int version, int versionnew) {
        db.execSQL("DROP TABLE IF EXISTS " + Constantes.TABLE);
        db.execSQL(sqlCreate);
    }
}
