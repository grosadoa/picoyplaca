package com.rp3.picoyplaca.Options.History;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;

import com.rp3.picoyplaca.Options.Find.FindSqlHelper;
import com.rp3.picoyplaca.R;
import com.rp3.picoyplaca.Utils.Constantes;

import java.util.ArrayList;
import java.util.List;

public class HistoryFragment extends Fragment {

    private SQLiteDatabase dbhistory;
    private List<HistoryModel> lstHistory = new ArrayList<>();
    private RecyclerView rv_menu_history;
    private TextView txtv_menu_history;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_history, container, false);
        startWidgets(root);
        return root;
    }

    private void startWidgets(View view){
        FindSqlHelper findhelper = new FindSqlHelper(getActivity(), Constantes.TABLE, null, Constantes.VERSIONTABLE);
        dbhistory = findhelper.getReadableDatabase();
        rv_menu_history = view.findViewById(R.id.rv_menu_history);
        txtv_menu_history = view.findViewById(R.id.txtv_menu_history);
        rv_menu_history.setLayoutManager(new LinearLayoutManager(getActivity()));
        ((SimpleItemAnimator) rv_menu_history.getItemAnimator()).setSupportsChangeAnimations(false);
        fillList();
    }

    private void fillList(){
        lstHistory = new ArrayList<HistoryModel>();
        Cursor c = dbhistory.rawQuery("SELECT "+Constantes.IMAGEFIND+", "+Constantes.TYPEFIND+" , "+Constantes.LICENSEPLATEFIND+" ,"+Constantes.DATEFIND+" ,"+Constantes.TIMEFIND+" ,"+Constantes.CONTRAVENTIONFIND+"  FROM " + Constantes.TABLE, null);
        if (c.moveToFirst()) {
            do {
                Integer image = c.getInt(0);
                String type = c.getString(1);
                String licenseplatestr = c.getString(2);
                String date = c.getString(3);
                String time = c.getString(4);
                String contravetion = c.getString(5);
                lstHistory.add(new HistoryModel(image,type,licenseplatestr,date,time,contravetion));
            } while(c.moveToNext());
        }
        if(lstHistory.isEmpty()){
            rv_menu_history.setVisibility(View.GONE);
            txtv_menu_history.setVisibility(View.VISIBLE);
        }
        else{
            rv_menu_history.setVisibility(View.VISIBLE);
            txtv_menu_history.setVisibility(View.GONE);
            initializeAdapter();
        }
    }

    void initializeAdapter(){
        HistoryAdapter adapter = new HistoryAdapter(getActivity(),lstHistory);
        rv_menu_history.setAdapter(adapter);
        rv_menu_history.setHasFixedSize(false);
    }
}