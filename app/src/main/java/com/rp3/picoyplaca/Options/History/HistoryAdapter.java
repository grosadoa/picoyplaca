package com.rp3.picoyplaca.Options.History;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import com.rp3.picoyplaca.R;
import com.rp3.picoyplaca.Utils.Constantes;

import java.util.List;


public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.ViewHolder> implements View.OnClickListener {


    List<HistoryModel> listData;
    static Context context;

    public HistoryAdapter(Context context, List<HistoryModel> develops) {
        this.listData = develops;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_menu_history, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        holder.setIsRecyclable(false);
        final HistoryModel item = listData.get(position);
        holder.bind(item);
    }


    @Override
    public int getItemCount() {
        return listData.size();
    }

    @Override
    public void onClick(View view){

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView txt_rowh_type,txt_rowh_licenseplate,txt_rowh_date,txt_rowh_time;
        ImageView img_rowh_transport;
        LinearLayout cdv_rowh_transport;

        public ViewHolder(View v) {
            super(v);
            img_rowh_transport = v.findViewById(R.id.img_rowh_transport);
            txt_rowh_type = v.findViewById(R.id.txt_rowh_type);
            txt_rowh_licenseplate = v.findViewById(R.id.txt_rowh_licenseplate);
            txt_rowh_date = v.findViewById(R.id.txt_rowh_date);
            txt_rowh_time = v.findViewById(R.id.txt_rowh_time);
            cdv_rowh_transport = v.findViewById(R.id.cdv_rowh_transport);
        }

        private void bind(HistoryModel model) {
            img_rowh_transport.setImageDrawable(context.getResources().getDrawable(model.getImage()));
            txt_rowh_type.setText(model.getType());
            txt_rowh_licenseplate.setText(model.getLicenseplate());
            txt_rowh_date.setText(model.getDate());
            txt_rowh_time.setText(model.getTime());
            if(model.getContravetion().equalsIgnoreCase(Constantes.YES)){
                cdv_rowh_transport.setBackgroundColor(context.getResources().getColor(R.color.colorRed));
            }
            else {
                cdv_rowh_transport.setBackgroundColor(context.getResources().getColor(R.color.colorPrimaryDark));
            }
        }
    }


}
