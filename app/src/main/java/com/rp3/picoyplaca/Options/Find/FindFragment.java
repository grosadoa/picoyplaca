package com.rp3.picoyplaca.Options.Find;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TimePicker;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.rp3.picoyplaca.Options.History.HistoryModel;
import com.rp3.picoyplaca.R;
import com.rp3.picoyplaca.Utils.Constantes;
import com.rp3.picoyplaca.Utils.Validate;

import java.util.Calendar;

public class FindFragment extends Fragment implements  View.OnClickListener{

    private RadioButton rdbtn_find_car,rdbtn_find_motorcycle;
    private EditText edt_find_license_plate,edt_find_date,edt_find_time;
    private Button btn_find;
    private String type;
    private static final String CERO = "0";
    private static final String BARRA = "/";
    private static final String DOS_PUNTOS = ":";
    private int hourselect = 0;
    private int minuteselect = 0;
    private int yearselect = 0;
    private int monthselect = 0;
    private int dayselect = 0;
    private String  licenseplate = "";
    private SQLiteDatabase dbfind;
    private Integer numbercontravention = 0;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_find, container, false);
            startWidgets(root);
        return root;
    }

    private void startWidgets(View view){
        rdbtn_find_car = view.findViewById(R.id.rdbtn_find_car);
        rdbtn_find_car.setOnClickListener(this);
        rdbtn_find_motorcycle = view.findViewById(R.id.rdbtn_find_motorcycle);
        rdbtn_find_motorcycle.setOnClickListener(this);
        edt_find_license_plate = view.findViewById(R.id.edt_find_license_plate);
        edt_find_license_plate.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
        edt_find_license_plate.setOnClickListener(this);
        edt_find_date = view.findViewById(R.id.edt_find_date);
        edt_find_date.setOnClickListener(this);
        edt_find_time = view.findViewById(R.id.edt_find_time);
        edt_find_time.setOnClickListener(this);
        btn_find = view.findViewById(R.id.btn_find);
        btn_find.setOnClickListener(this);
        type="";
        FindSqlHelper findhelper = new FindSqlHelper(getActivity(), Constantes.TABLE, null, Constantes.VERSIONTABLE);
        dbfind = findhelper.getWritableDatabase();
    }

    public boolean validatefields(){
        licenseplate = edt_find_license_plate.getText().toString().trim();
        String date = edt_find_date.getText().toString().trim();
        String time = edt_find_time.getText().toString().trim();
        if(type.isEmpty()){
            edt_find_license_plate.setError(getText(R.string.validation_radio_empty));
            return false;
        }
        if(licenseplate.isEmpty()){
            edt_find_license_plate.setError(getText(R.string.validation_field_empty));
            return false;
        }
        if(date.isEmpty()){
            edt_find_date.setError(getText(R.string.validation_field_empty));
            return false;
        }
        if(time.isEmpty()){
            edt_find_time.setError(getText(R.string.validation_field_empty));
            return false;
        }
        if(type.equalsIgnoreCase(Constantes.CAR)){
            if(!Validate.validateCarsLicensePlate(licenseplate)){
                edt_find_license_plate.setError(getText(R.string.validation_field_type_licence_plate));
                return false;
            }
        }
        else{
            if(!Validate.validateMotorcyclesLicensePlate( licenseplate)){
                edt_find_license_plate.setError(getText(R.string.validation_field_type_licence_plate));
                return false;
            }
        }
        return true;
    }

    private void selectiondate(){
        Calendar c = Calendar.getInstance();
        int monthnow = c.get(Calendar.MONTH);
        int daynow = c.get(Calendar.DAY_OF_MONTH);
        int yearnow = c.get(Calendar.YEAR);
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                final int monthnowpicker = month + 1;
                String dayformat = (dayOfMonth < 10)? CERO + String.valueOf(dayOfMonth):String.valueOf(dayOfMonth);
                String monthformat = (monthnowpicker < 10)? CERO + String.valueOf(monthnowpicker):String.valueOf(monthnowpicker);
                edt_find_date.setText(dayformat + BARRA + monthformat + BARRA + year);
                yearselect = year;
                monthselect = month;
                dayselect = dayOfMonth;
            }
        },yearnow, monthnow, daynow);
        datePickerDialog.show();
    }

    private void selectiontime(){
        Calendar c = Calendar.getInstance();
        int hournow = c.get(Calendar.HOUR_OF_DAY);
        int minutenow = c.get(Calendar.MINUTE);
        TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                String hourformat =  (hourOfDay < 10)? String.valueOf(CERO + hourOfDay) : String.valueOf(hourOfDay);
                String minuteformat = (minute < 10)? String.valueOf(CERO + minute):String.valueOf(minute);
                String AM_PM;
                if(hourOfDay < 12) {
                    AM_PM = "A.M.";
                } else {
                    AM_PM = "P.M.";
                }
                edt_find_time.setText(hourformat + DOS_PUNTOS + minuteformat + " " + AM_PM);
                minuteselect = minute;
                hourselect = hourOfDay;
            }
        }, hournow, minutenow, false);
        timePickerDialog.show();
    }

    @Override
    public void onClick(View v) {
        int position = v.getId();
        if(btn_find.getId()==position){
            if(validatefields()){
                numbercontravetionLicensePlate();
            }
        }
        if(rdbtn_find_car.getId()==position){
            if (rdbtn_find_car.isChecked()){
                type = Constantes.CAR;
            }
        }
        if(rdbtn_find_motorcycle.getId()==position){
            if (rdbtn_find_motorcycle.isChecked()){
                type = Constantes.MOTORCYCLE;
            }
        }
        if(edt_find_date.getId()==position){
            selectiondate();
        }
        if(edt_find_time.getId()==position){
            selectiontime();
        }
    }

    private void numbercontravetionLicensePlate(){
        Cursor c = dbfind.rawQuery("SELECT COUNT("+Constantes.LICENSEPLATEFIND+")  FROM " + Constantes.TABLE +" WHERE "+Constantes.LICENSEPLATEFIND + " = '" + licenseplate +"' AND " + Constantes.CONTRAVENTIONFIND + " = '" + Constantes.YES +"'", null);
        if (c.moveToFirst()) {
            do {
                numbercontravention = c.getInt(0);
            } while(c.moveToNext());
        }
        Log.e("ERROR","numero "+numbercontravention);
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, yearselect);
        calendar.set(Calendar.MONTH, monthselect);
        calendar.set(Calendar.DAY_OF_MONTH, dayselect);
        calendar.set(Calendar.HOUR_OF_DAY, hourselect);
        calendar.set(Calendar.MINUTE, minuteselect);
        Boolean contravention = Validate.validatePicoPlaca(calendar,licenseplate,type);
        Intent intent = new Intent(getActivity(),FindDetailActivity.class);
        intent.putExtra(Constantes.IMAGEFIND,type.equalsIgnoreCase(Constantes.CAR) ? R.drawable.ic_car:R.drawable.ic_motorcycle);
        intent.putExtra(Constantes.TYPEFIND,type);
        intent.putExtra(Constantes.LICENSEPLATEFIND,licenseplate);
        intent.putExtra(Constantes.DATEFIND,edt_find_date.getText().toString().trim());
        intent.putExtra(Constantes.TIMEFIND,edt_find_time.getText().toString().trim());
        intent.putExtra(Constantes.CONTRAVENTIONFIND,contravention ? Constantes.YES : Constantes.NO);
        intent.putExtra(Constantes.CONTRAVENTIONNUMBER,numbercontravention);
        ContentValues newRecord = new ContentValues();
        newRecord.put(Constantes.IMAGEFIND, type.equalsIgnoreCase(Constantes.CAR) ? R.drawable.ic_car:R.drawable.ic_motorcycle);
        newRecord.put(Constantes.TYPEFIND, type);
        newRecord.put(Constantes.LICENSEPLATEFIND, licenseplate);
        newRecord.put(Constantes.DATEFIND, edt_find_date.getText().toString().trim());
        newRecord.put(Constantes.TIMEFIND, edt_find_time.getText().toString().trim());
        newRecord.put(Constantes.CONTRAVENTIONFIND, contravention ? Constantes.YES : Constantes.NO);
        dbfind.insert(Constantes.TABLE, null, newRecord);
        startActivity(intent);
    }

}