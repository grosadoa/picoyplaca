package com.rp3.picoyplaca.Options.Find;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.rp3.picoyplaca.R;
import com.rp3.picoyplaca.Utils.Constantes;

public class FindDetailActivity extends AppCompatActivity {

    private TextView txt_finddt_type,txt_finddt_license_plate,txt_finddt_date,txt_finddt_license_time,txt_finddt_contravention,txt_finddt_contravetion_previous;
    private ImageView img_finddt_type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_detail);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        startWidgets();
    }

    private void startWidgets(){
        txt_finddt_type = findViewById(R.id.txt_finddt_type);
        txt_finddt_type.setText(getIntent().getExtras().getString(Constantes.TYPEFIND)!= null ? getIntent().getExtras().getString(Constantes.TYPEFIND) : "");
        txt_finddt_license_plate = findViewById(R.id.txt_finddt_license_plate);
        txt_finddt_license_plate.setText(getIntent().getExtras().getString(Constantes.LICENSEPLATEFIND)!= null ? getIntent().getExtras().getString(Constantes.LICENSEPLATEFIND) : "");
        txt_finddt_date = findViewById(R.id.txt_finddt_date);
        txt_finddt_date.setText(getIntent().getExtras().getString(Constantes.DATEFIND)!= null ? getIntent().getExtras().getString(Constantes.DATEFIND) : "");
        txt_finddt_license_time = findViewById(R.id.txt_finddt_license_time);
        txt_finddt_license_time.setText(getIntent().getExtras().getString(Constantes.TIMEFIND)!= null ? getIntent().getExtras().getString(Constantes.TIMEFIND) : "");
        txt_finddt_contravention = findViewById(R.id.txt_finddt_contravention);
        String contravention = getIntent().getExtras().getString(Constantes.CONTRAVENTIONFIND)!= null ? getIntent().getExtras().getString(Constantes.CONTRAVENTIONFIND) : "";
        txt_finddt_contravention.setText(contravention);
        if (contravention.equalsIgnoreCase(Constantes.YES)){
            txt_finddt_contravention.setTextColor(getResources().getColor(R.color.colorRed));
        }
        else {
            txt_finddt_contravention.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        }
        img_finddt_type = findViewById(R.id.img_finddt_type);
        img_finddt_type.setImageDrawable(getResources().getDrawable(getIntent().getExtras().getInt(Constantes.IMAGEFIND)));
        txt_finddt_contravetion_previous = findViewById(R.id.txt_finddt_contravetion_previous);
        txt_finddt_contravetion_previous.setText(""+getIntent().getExtras().getInt(Constantes.CONTRAVENTIONNUMBER));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home)
        {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
