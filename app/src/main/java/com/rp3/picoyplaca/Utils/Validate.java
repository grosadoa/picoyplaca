package com.rp3.picoyplaca.Utils;

import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validate {
    private static final String CARS = "^([A-Z]{3}\\d{3,4})$";
    private static final String MOTORCYCLE = "^([A-Z]{2}\\d{3}[A-Z])$";
    private static Pattern pattern;
    private static Matcher matcher;

    public static Boolean validateCarsLicensePlate(String licensePlate){
        pattern = Pattern.compile(CARS);
        matcher = pattern.matcher(licensePlate);
        return matcher.matches();
    }

    public static Boolean validateMotorcyclesLicensePlate(String licensePlate){
        pattern = Pattern.compile(MOTORCYCLE);
        matcher = pattern.matcher(licensePlate);
        return matcher.matches();
    }

    public static Boolean validatePicoPlaca(Calendar calendar, String platelicense, String type){
        Boolean datareturn = false;
        int dayokweek =  calendar.get(Calendar.DAY_OF_WEEK);
        Date datetime = calendar.getTime();
        Integer lastcharacter = 0;
        if(type.equalsIgnoreCase(Constantes.CAR)){
            lastcharacter =  Integer.parseInt(platelicense.substring(platelicense.length() - 1));
        }
        else{
            String platelicensenow= platelicense.substring(0,platelicense.length() - 1);
            lastcharacter =  Integer.parseInt(platelicensenow.substring(platelicensenow.length() - 1));
        }
        SimpleDateFormat dateFormathours = new SimpleDateFormat("HH");
        SimpleDateFormat dateFormatminute = new SimpleDateFormat("mm");
        Integer hoursint = new Integer(dateFormathours.format(datetime));
        Integer minuteint = new Integer(dateFormatminute.format(datetime));
        switch (dayokweek){
            case Calendar.MONDAY:
                if(lastcharacter == 1 || lastcharacter == 2){
                    datareturn = validateHoursMinutePicoPlaca(hoursint,minuteint);
                }
                break;
            case Calendar.TUESDAY:
                if(lastcharacter == 3 || lastcharacter == 4){
                    datareturn = validateHoursMinutePicoPlaca(hoursint,minuteint);
                }
                break;
            case Calendar.WEDNESDAY:
                if(lastcharacter == 5 || lastcharacter == 6){
                    datareturn = validateHoursMinutePicoPlaca(hoursint,minuteint);
                }
                break;
            case Calendar.THURSDAY:
                if(lastcharacter == 7 || lastcharacter == 8){
                    datareturn = validateHoursMinutePicoPlaca(hoursint,minuteint);
                }
                break;
            case Calendar.FRIDAY:
                if(lastcharacter == 9 || lastcharacter == 0){
                    datareturn = validateHoursMinutePicoPlaca(hoursint,minuteint);
                }
                break;
        }
        return datareturn;
    }

    public  static  Boolean validateHoursMinutePicoPlaca(Integer hour, Integer minute){
        Boolean contravention = false;
        if(hour >= 7 && hour <= 9){
                if(minute <= 30){
                    contravention = true;
                }
        }
        else{
            if(hour >= 16 && hour <= 19){
                if(minute <= 30){
                    contravention = true;
                }
            }
        }
        return contravention;
    }

}
