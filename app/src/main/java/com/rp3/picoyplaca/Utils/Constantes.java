package com.rp3.picoyplaca.Utils;

public class Constantes {

    public static Integer DURACION_SPLASH = 2000;
    public static String CAR = "AUTOMÓVIL";
    public static String MOTORCYCLE = "MOTO";
    public static String YES = "SI";
    public static String NO = "NO";
    public static String IMAGEFIND = "IMAGEFIND";
    public static String TYPEFIND = "TYPEFIND";
    public static String LICENSEPLATEFIND = "LICENSEPLATEFIND";
    public static String DATEFIND = "DATEFIND";
    public static String TIMEFIND = "TIMEFIND";
    public static String CONTRAVENTIONFIND = "CONTRAVENTIONFIND";
    public static String TABLE = "PICOPLACA";
    public static Integer VERSIONTABLE = 1;
    public static String CONTRAVENTIONNUMBER = "CONTRAVENTIONNUMBER";
}
